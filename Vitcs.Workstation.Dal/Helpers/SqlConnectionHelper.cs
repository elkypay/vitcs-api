﻿using System.Data;
using System.Data.SqlClient;

namespace Vitcs.Workstation.Dal.Helpers
{
    internal static class SqlConnectionHelper
    {
        public static IDbConnection GetDbConnection(string connectionString) => new SqlConnection(connectionString);
    }
}