﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Dapper;

namespace Vitcs.Workstation.Dal.Helpers
{
    public static class TableValuedParameterHelper
    {
        public static SqlMapper.ICustomQueryParameter AsTableValuedParameter(this IEnumerable<int> integers) =>
            integers.AsTableValuedParameter("IntArray");

        public static SqlMapper.ICustomQueryParameter AsTableValuedParameter(this IEnumerable<DateTime> dates) =>
            dates.AsTableValuedParameter("DateTime2Array");

        public static SqlMapper.ICustomQueryParameter AsTableValuedParameter<T>(this IEnumerable<T> values, string sqlTypeName)
        {
            Type type = typeof(T);
            bool isPrimitive = type == typeof(int)
                || type == typeof(DateTime);
            PropertyInfo[] properties = type.GetProperties();

            DataTable dataTable = new DataTable();

            if (isPrimitive)
                dataTable.Columns.Add("Value", type);
            else
            {
                foreach (PropertyInfo property in properties)
                    dataTable.Columns.Add(property.Name, property.PropertyType);
            }

            foreach (T value in values)
            {
                if (isPrimitive)
                    dataTable.Rows.Add(value);
                else
                    dataTable.Rows.Add(properties.Select(property => property.GetValue(value)).ToArray());
            }

            return dataTable.AsTableValuedParameter(sqlTypeName);
        }
    }
}