﻿using System.Linq;
using Dapper;

namespace Vitcs.Workstation.Dal.Helpers
{
    internal static class DynamicParametersHelpers
    {
        public static DynamicParameters ToDynamicParameters(this object obj) =>
            new DynamicParameters(obj.GetType().GetProperties().ToDictionary(p => p.Name, p => p.GetValue(obj)));
    }
}