ALTER PROCEDURE GetStep1QueueData
AS
BEGIN
    DECLARE @freeSkidCount INT;

    SELECT @freeSkidCount = COUNT(*)
    FROM Stations
    WHERE [Status] = 'Idle';

    SELECT QueueNumber
    FROM Steps
    INNER JOIN Step1Activities step1 ON step1.Id = Step1ActivityId
    WHERE Step2ActivityId IS NULL
    ORDER BY QueueNumber
    OFFSET @freeSkidCount ROWS
    FETCH NEXT 6 ROWS ONLY;

    SELECT TOP (@freeSkidCount) QueueNumber
    FROM Steps
    INNER JOIN Step1Activities step1 ON step1.Id = Step1ActivityId
    WHERE Step2ActivityId IS NULL
    ORDER BY QueueNumber;
END