ALTER PROCEDURE GetStep3ActivityOilData
    @id INT
AS
BEGIN
    DECLARE
        @minQuantity INT = 0,
        @maxQuantity INT = 100,
        @minTemperature INT = 0,
        @maxTemperature INT = 100,
        @minDensity INT = 0,
        @maxDensity INT = 100;

    DECLARE
        @quantity INT,
        @temperature INT,
        @density INT,
        @stationStatus NVARCHAR(255);

     SELECT @quantity = ABS(CHECKSUM(NEWID()) % (@maxQuantity - @minQuantity + 1)) + @minQuantity,
        @temperature = ABS(CHECKSUM(NEWID()) % (@maxTemperature - @minTemperature + 1)) + @minTemperature,
        @density = ABS(CHECKSUM(NEWID()) % (@maxDensity - @minDensity + 1)) + @minDensity,
        @stationStatus = CASE WHEN ABS(CHECKSUM(NEWID()) % 11) = 0 THEN 'Idle' ELSE 'In Progress' END;

    SELECT @quantity AS Quantity,
        @temperature AS Temperature,
        @density AS Density,
        @stationStatus AS StationStatus;

    IF @stationStatus = 'Idle'
    BEGIN
        UPDATE Step3Activities
        SET CheckOut = GETUTCDATE(),
            Quantity = @quantity,
            Temperature = @temperature,
            Density = @density
        WHERE Id = @id;

        DECLARE @step1ActivityId INT;

        SELECT TOP 1 @step1ActivityId = step1.Id
        FROM Steps
        INNER JOIN Step1Activities step1 ON step1.Id = Steps.Step1ActivityId
        WHERE step1.DriverId = (SELECT DriverId FROM Step3Activities WHERE Id = @id)
            AND Step2ActivityId IS NOT NULL
            AND Step3ActivityId IS NULL
        ORDER BY [CheckIn] DESC;

        UPDATE Steps
        SET Step3ActivityId = @id
        WHERE Step1ActivityId = @step1ActivityId;
    END
END