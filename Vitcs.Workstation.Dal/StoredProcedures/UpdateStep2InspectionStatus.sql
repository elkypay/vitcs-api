ALTER PROCEDURE UpdateStep2InspectionStatus
    @step2ActivityId INT,
    @inspectorId INT,
    @inspectionStatus NVARCHAR(255),
    @inspectionComment NVARCHAR(255)
AS
BEGIN
    UPDATE Step2Activities
    SET InspectorId = @inspectorId,
        InspectionStatus = @inspectionStatus,
        InspectionComment = @inspectionComment
    WHERE Id = @step2ActivityId;

    IF @inspectionStatus = 'Access Denied'
        RETURN;

    DECLARE @passIds TABLE (Id INT);
    DECLARE @passId INT;

    INSERT INTO Passes
    OUTPUT INSERTED.Id INTO @passIds 
    DEFAULT VALUES;

    SELECT @passId = Id
    FROM @passIds;

    UPDATE Step2Activities
    SET CheckOut = GETUTCDATE(),
        PrintedPassId = @passId
    WHERE Id = @step2ActivityId;

    DECLARE @step1ActivityId INT;

    SELECT TOP 1 @step1ActivityId = step1.Id
    FROM Steps
    INNER JOIN Step1Activities step1 ON step1.Id = Steps.Step1ActivityId
    WHERE step1.DriverId = (SELECT DriverId FROM Step2Activities WHERE Id = @step2ActivityId)
        AND Step2ActivityId IS NULL
    ORDER BY [CheckIn] DESC;

    UPDATE Steps
    SET Step2ActivityId = @step2ActivityId
    WHERE Step1ActivityId = @step1ActivityId;
END