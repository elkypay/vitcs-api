ALTER PROCEDURE UpdateStep1ActivityWithWaybill
    @step1ActivityId INT,
    @waybill NVARCHAR(255),
    @waybillInputMethod NVARCHAR(255)
AS
BEGIN
    DECLARE @waybillId INT = NULL;

    SELECT @waybillId = Id
    FROM Waybills
    WHERE TagId = @waybill;

    DECLARE @isExpiredWaybill BIT;
    SELECT @isExpiredWaybill = CASE WHEN DATEADD(WEEK, 1, [Timestamp]) < GETUTCDATE() THEN 1 ELSE 0 END
    FROM Waybills
    WHERE Id = @waybillId;

    UPDATE Step1Activities
    SET WaybillId = @waybillId,
        EnteredWaybill = @waybill,
        WaybillInputMethod = @waybillInputMethod,
        IsExpiredWaybill = @isExpiredWaybill
    WHERE Id = @step1ActivityId;

    IF (@waybillId IS NULL)
    BEGIN
        SELECT 0 AS IsSuccess,
            'WRONG_WAYBILL_NUMBER' AS ErrorMessage;
        RETURN;
    END

    IF (@isExpiredWaybill = 1)
    BEGIN
        SELECT 0 AS IsSuccess,
            'EXPIRED_WAYBILL' AS ErrorMessage;
        RETURN;
    END

    DECLARE @driverId INT;

    SELECT @driverId = DriverId
    FROM Step1Activities
    WHERE Id = @step1ActivityId;

    DECLARE @passIds TABLE (Id INT);
    DECLARE @passId INT;

    INSERT INTO Passes
    OUTPUT INSERTED.Id INTO @passIds 
    DEFAULT VALUES;

    SELECT @passId = Id
    FROM @passIds;

    DECLARE @queueNumber INT;
    SELECT @queueNumber = (CASE WHEN MAX(QueueNumber) IS NULL THEN 1 ELSE MAX(QueueNumber) + 1 END)
    FROM Step1Activities;

    UPDATE Step1Activities
    SET CheckOut = GETUTCDATE(),
        QueueNumber = @queueNumber,
        PrintedPassId = @passId
    WHERE Id = @step1ActivityId;

    INSERT INTO Steps
    (Step1ActivityId)
    VALUES
    (@step1ActivityId);

    SELECT 1 AS IsSuccess,
        NULL AS ErrorMessage;
END