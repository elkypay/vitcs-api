ALTER PROCEDURE CreateStep5Activity
    @pass NVARCHAR(255),
    @isManuallyEnteredPass BIT
AS
BEGIN
    DECLARE @passId INT = PARSE(@pass AS INT);
    DECLARE @driverId INT = NULL;

    SELECT @driverId = DriverId
    FROM Step1Activities
    WHERE PrintedPassId = @passId;

    SELECT @driverId = DriverId
    FROM Step2Activities
    WHERE PrintedPassId = @passId;

    INSERT INTO Step5Activities
    (PassId, DriverId, EnteredPass, IsManuallyEnteredPass)
    OUTPUT CASE WHEN @driverId IS NULL THEN NULL ELSE INSERTED.Id END
    VALUES
    (@passId, @driverId, @pass, @isManuallyEnteredPass);

    SELECT FullName
    FROM Drivers
    WHERE Id = @driverId;
END