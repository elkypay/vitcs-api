ALTER PROCEDURE CreateStep3ActivityUsingSecurityQuestions
    @stationId INT,
    @iqamaNo NVARCHAR(255),
    @licenseNo NVARCHAR(255),
    @country NVARCHAR(255)
AS
BEGIN
    DECLARE @driverId INT = NULL;

    SELECT @driverId = Id
    FROM Drivers
    WHERE IqamaNo = @iqamaNo
        AND LicenseNo = @licenseNo
        AND Country = @country;

    INSERT INTO Step3Activities
    (StationId, DriverId, SecurityQuestionIqamaNo, SecurityQuestionLicenseNo, SecurityQuestionCountry)
    OUTPUT CASE WHEN @driverId IS NULL THEN NULL ELSE INSERTED.ID END
    VALUES
    (@stationId, @driverId, @iqamaNo, @licenseNo, @country);

    SELECT FullName
    FROM Drivers
    WHERE Id = @driverId;
END