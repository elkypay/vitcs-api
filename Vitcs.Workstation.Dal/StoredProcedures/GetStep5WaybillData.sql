ALTER PROCEDURE GetStep5WaybillData
    @id INT
AS
BEGIN
    DECLARE @passId INT;

    SELECT @passId = PassId
    FROM Step5Activities
    WHERE Id = @id;

    SELECT waybill.Id AS WaybillId,
        waybill.TagId AS WaybillBarcode,
        contractor.[Name] AS ContractorName,
        driver.FullName AS DriverFullName,
        step1.CheckIn AS Step1CheckIn,
        step3.CheckOut AS Step3CheckOut,
        step3.Quantity AS Step3Quantity 
    FROM Steps
    INNER JOIN Step1Activities step1 ON step1.Id = Steps.Step1ActivityId
    INNER JOIN Step2Activities step2 ON step2.Id = Steps.Step2ActivityId
    INNER JOIN Step3Activities step3 ON step3.Id = Steps.Step3ActivityId
    INNER JOIN Drivers driver ON driver.Id = step1.DriverId
    INNER JOIN Contractors contractor ON driver.ContractorId = contractor.Id
    INNER JOIN Waybills waybill ON step1.WaybillId = waybill.Id
    WHERE step1.PrintedPassId = @passId OR step2.PrintedPassId = @passId;

    UPDATE Step5Activities
    SET CheckOut = GETUTCDATE()
    WHERE Id = @id;

    DECLARE @step1ActivityId INT;

    SELECT TOP 1 @step1ActivityId = step1.Id
    FROM Steps
    INNER JOIN Step1Activities step1 ON step1.Id = Steps.Step1ActivityId
    WHERE step1.DriverId = (SELECT DriverId FROM Step5Activities WHERE Id = @id)
        AND Step2ActivityId IS NOT NULL
        AND Step3ActivityId IS NOT NULL
        AND Step5ActivityId IS NULL
    ORDER BY [CheckIn] DESC;

    UPDATE Steps
    SET Step5ActivityId = @id
    WHERE Step1ActivityId = @step1ActivityId;
END