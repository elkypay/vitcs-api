ALTER PROCEDURE CreateStep1Activity
    @accessCard NVARCHAR(255),
    @isManuallyEnteredAccessCard BIT,
    @fingerprintId NVARCHAR(255)
AS
BEGIN
    DECLARE @fingerprintMode BIT;

    SELECT @fingerprintMode = FingerprintMode
    FROM AuthenticationMode;

    DECLARE @driverId INT = NULL;

    SELECT @driverId = Id
    FROM Drivers
    WHERE TagId = @accessCard
        AND (@fingerprintMode = 0 OR FingerprintId IS NULL OR FingerprintId = @fingerprintId);

    IF (@fingerprintMode = 1)
    BEGIN
        UPDATE Drivers
        SET FingerprintId = @fingerprintId
        WHERE Id = @driverId AND FingerprintId IS NULL;
    END

    INSERT INTO Step1Activities
    (DriverId, EnteredAccessCard, IsManuallyEnteredAccessCard, EnteredFingerprintId)
    OUTPUT CASE WHEN @driverId IS NULL THEN NULL ELSE INSERTED.Id END
    VALUES
    (@driverId, @accessCard, @isManuallyEnteredAccessCard, @fingerprintId);

    SELECT FullName
    FROM Drivers
    WHERE Id = @driverId;
END