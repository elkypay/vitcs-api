ALTER PROCEDURE CreateStep1ActivityUsingSecurityQuestions
    @iqamaNo NVARCHAR(255),
    @licenseNo NVARCHAR(255),
    @country NVARCHAR(255)
AS
BEGIN
    DECLARE @driverId INT = NULL;

    SELECT @driverId = Id
    FROM Drivers
    WHERE IqamaNo = @iqamaNo
        AND LicenseNo = @licenseNo
        AND Country = @country;

    INSERT INTO Step1Activities
    (DriverId, SecurityQuestionIqamaNo, SecurityQuestionLicenseNo, SecurityQuestionCountry)
    OUTPUT CASE WHEN @driverId IS NULL THEN NULL ELSE INSERTED.ID END
    VALUES
    (@driverId, @iqamaNo, @licenseNo, @country);

    SELECT FullName
    FROM Drivers
    WHERE Id = @driverId;
END