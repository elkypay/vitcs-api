ALTER PROCEDURE GetStep2PrintedPassData
    @id INT
AS
BEGIN
    SELECT FORMAT(step2.Id, 'D10') AS Barcode,
        d.FullName AS DriverFullName,
        t.TagId AS TruckTag,
        tc.TagId AS TruckContainerTag,
        step1.Waybill,
        CheckOut,
        step2.InspectionStatus,
        step2.InspectionComment
    FROM Step2Activities step2
    INNER JOIN Drivers d ON d.Id = DriverId
    INNER JOIN Trucks t ON t.Id = TruckId
    INNER JOIN TruckContainers tc ON tc.Id = TruckContainerId
    OUTER APPLY
    (
        SELECT TOP 1 w.TagId AS Waybill
        FROM Step1Activities
        INNER JOIN Waybills w ON w.Id = WaybillId
        WHERE d.Id = Step1Activities.DriverId
        ORDER BY [Timestamp] DESC
    ) step1
    WHERE step2.Id = @id;
END