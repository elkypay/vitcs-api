ALTER PROCEDURE Reports.GetStep2InspectionStatus
    @step2ActivityId INT
AS
BEGIN
    SELECT InspectionStatus
    FROM Step2Activities
    WHERE Id = @step2ActivityId;
END