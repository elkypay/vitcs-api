ALTER PROCEDURE CreateStep2Activity
    @accessCard NVARCHAR(255),
    @isManuallyEnteredAccessCard BIT
AS
BEGIN
    DECLARE @driverId INT = NULL;

    SELECT @driverId = Id
    FROM Drivers
    WHERE TagId = @accessCard;

    INSERT INTO Step2Activities
    (DriverId, EnteredAccessCard, IsManuallyEnteredAccessCard)
    OUTPUT CASE WHEN @driverId IS NULL THEN NULL ELSE INSERTED.Id END
    VALUES
    (@driverId, @accessCard, @isManuallyEnteredAccessCard);

    SELECT FullName
    FROM Drivers
    WHERE Id = @driverId;
END