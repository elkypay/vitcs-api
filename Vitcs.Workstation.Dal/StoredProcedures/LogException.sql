ALTER PROCEDURE LogException
    @exceptions ExceptionTableType READONLY
AS
BEGIN
    DECLARE @cursor CURSOR;

    DECLARE @exceptionSource NVARCHAR(255);
    DECLARE @route NVARCHAR(255);
    DECLARE @type NVARCHAR(255);
    DECLARE @message NVARCHAR(MAX);
    DECLARE @stackTrace NVARCHAR(MAX);

    DECLARE @innerExceptionId INT = NULL;

    DECLARE @outputTable TABLE (Id INT);

    BEGIN
        SET @cursor = CURSOR FOR
        SELECT ExceptionSource, [Route], [Type], [Message], StackTrace
        FROM @exceptions;

        OPEN @cursor
        FETCH NEXT FROM @cursor
        INTO @exceptionSource, @route, @type, @message, @stackTrace;

        WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO Exceptions
            (ExceptionSource, [Route], [Type], [Message], StackTrace, InnerExceptionId)
            OUTPUT INSERTED.Id INTO @outputTable (Id)
            VALUES
            (@exceptionSource, @route, @type, @message, @stackTrace, @innerExceptionId);

            SELECT @innerExceptionId = Id
            FROM @outputTable;

            FETCH NEXT FROM @cursor
            INTO @exceptionSource, @route, @type, @message, @stackTrace;
        END;

        CLOSE @cursor;
        DEALLOCATE @cursor;
    END
END