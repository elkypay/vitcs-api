ALTER PROCEDURE GetStep2InspectorData
    @login NVARCHAR(255),
    @password NVARCHAR(255)
AS
BEGIN
    SELECT Id, FullName
    FROM Inspectors
    WHERE [Login] = @login AND [Password] = @password;
END