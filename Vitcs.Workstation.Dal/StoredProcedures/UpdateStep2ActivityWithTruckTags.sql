ALTER PROCEDURE UpdateStep2ActivityWithTruckTags
    @step2ActivityId INT,
    @truckTag NVARCHAR(255),
    @truckContainerTag NVARCHAR(255),
    @isManuallyEnteredTruckTags bit
AS
BEGIN
    DECLARE @driverId INT;

    SELECT @driverId = DriverId
    FROM Step2Activities
    WHERE Id = @step2ActivityId;

    DECLARE @truckId INT = NULL;

    SELECT @truckId = Trucks.Id
    FROM Trucks
    INNER JOIN Drivers ON Drivers.ContractorId = Trucks.ContractorId
    WHERE Trucks.TagId = @truckTag AND Drivers.Id = @driverId;

    DECLARE @truckContainerId INT = NULL;

    SELECT @truckContainerId = TruckContainers.Id
    FROM TruckContainers
    INNER JOIN Drivers ON Drivers.ContractorId = TruckContainers.ContractorId
    WHERE TruckContainers.TagId = @truckContainerTag AND Drivers.Id = @driverId;

    UPDATE Step2Activities
    SET EnteredTruckTag = @truckTag,
        EnteredTruckContainerTag = @truckContainerTag,
        IsManuallyEnteredTruckTags = @isManuallyEnteredTruckTags
    WHERE Id = @step2ActivityId;

    IF (@truckId IS NOT NULL AND @truckContainerId IS NOT NULL)
    BEGIN
        UPDATE Step2Activities
        SET TruckId = @truckId,
            TruckContainerId = @truckContainerId
        WHERE Id = @step2ActivityId;

        SELECT 1;
    END
    ELSE
        SELECT 0;
END