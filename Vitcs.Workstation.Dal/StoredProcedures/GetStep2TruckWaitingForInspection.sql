ALTER PROCEDURE GetStep2TruckWaitingForInspection
    @driverTagId NVARCHAR(255),
    @driverFullName NVARCHAR(255),
    @driverIqamaNo NVARCHAR(255),
    @truckTagId NVARCHAR(255),
    @truckContainerTagId NVARCHAR(255)
AS
BEGIN
    SELECT TOP 1 step2.Id
    FROM Step2Activities step2
    INNER JOIN Drivers driver ON DriverId = driver.Id
    INNER JOIN Trucks truck ON TruckId = truck.Id
    INNER JOIN TruckContainers truckContainer ON TruckContainerId = truckContainer.Id
    WHERE InspectionStatus IS NULL
        AND driver.TagId = @driverTagId
        AND driver.FullName = @driverFullName
        AND driver.IqamaNo = @driverIqamaNo
        AND truck.TagId = @truckTagId
        AND truckContainer.TagId = @truckContainerTagId
    ORDER BY CheckIn DESC;
END