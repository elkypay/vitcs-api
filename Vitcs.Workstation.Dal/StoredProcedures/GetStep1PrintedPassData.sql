ALTER PROCEDURE GetStep1PrintedPassData
    @id INT
AS
BEGIN
    SELECT FORMAT(s.Id, 'D10') AS Barcode,
        d.FullName AS DriverFullName,
        w.TagId AS Waybill,
        QueueNumber,
        CheckOut
    FROM Step1Activities s
    INNER JOIN Drivers d ON d.Id = DriverId
    INNER JOIN Waybills w ON w.Id = WaybillId
    WHERE s.Id = @id;
END