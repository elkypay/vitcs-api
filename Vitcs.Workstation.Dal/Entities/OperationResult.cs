﻿namespace Vitcs.Workstation.Dal.Entities
{
    public class OperationResult
    {
        public bool IsSuccess { get; set; }

        public string ErrorMessage { get; set; }
    }
}