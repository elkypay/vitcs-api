﻿namespace Vitcs.Workstation.Dal.Entities
{
    public class AuthenticationMode
    {
        public bool FingerprintMode { get; set; }
    }
}