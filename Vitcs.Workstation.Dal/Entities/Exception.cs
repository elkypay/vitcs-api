﻿namespace Vitcs.Workstation.Dal.Entities
{
    public class Exception
    {
        public string ExceptionSource { get; set; }

        public string Route { get; set; }

        public string Type { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }
    }
}