﻿using System.Collections.Generic;

namespace Vitcs.Workstation.Dal.Entities.Complex
{
    public class Step1QueueData
    {
        public IEnumerable<int> PendingQueueNumbers { get; set; }

        public IEnumerable<int> ReadyQueueNumbers { get; set; }
    }
}