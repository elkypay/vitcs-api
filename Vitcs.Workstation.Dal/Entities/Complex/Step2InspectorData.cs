﻿namespace Vitcs.Workstation.Dal.Entities.Complex
{
    public class Step2InspectorData
    {
        public int Id { get; set; }

        public string FullName { get; set; }
    }
}