﻿namespace Vitcs.Workstation.Dal.Entities.Complex
{
    public class Step3ActivityOilData
    {
        public int Quantity { get; set; }

        public int Temperature { get; set; }

        public int Density { get; set; }

        public string StationStatus { get; set; }
    }
}