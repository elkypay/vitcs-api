﻿namespace Vitcs.Workstation.Dal.Entities.Complex
{
    public class Step3Activity
    {
        public int? Id { get; set; }

        public string DriverFullName { get; set; }
    }
}