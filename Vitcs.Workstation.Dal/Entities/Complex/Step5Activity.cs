﻿namespace Vitcs.Workstation.Dal.Entities.Complex
{
    public class Step5Activity
    {
        public int? Id { get; set; }

        public string DriverFullName { get; set; }
    }
}