﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vitcs.Workstation.Dal.Entities;

namespace Vitcs.Workstation.Dal.Abstract
{
    public interface IExceptionRepository
    {
        Task SaveException(IEnumerable<Exception> exceptions);
    }
}