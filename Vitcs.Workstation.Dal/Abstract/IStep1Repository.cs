﻿using System.Threading.Tasks;
using Vitcs.Workstation.Dal.Entities;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.QueryParameters.Step1;

namespace Vitcs.Workstation.Dal.Abstract
{
    public interface IStep1Repository
    {
        Task<Step1Activity> CreateStep1Activity(Step1Credentials step1Credentials);

        Task<Step1Activity> CreateStep1ActivityUsingSecurityQuestions(Step1SecurityQuestions step1SecurityQuestions);

        Task<OperationResult> UpdateStep1ActivityWithWaybill(Step1WaybillParameters step1WaybillParameters);

        Task<Step1PrintedPassData> GetStep1PrintedPassData(int id);
    }
}