﻿using System.Threading.Tasks;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.QueryParameters.Step2;

namespace Vitcs.Workstation.Dal.Abstract
{
    public interface IStep2Repository
    {
        Task<Step2Activity> CreateStep2Activity(Step2Credentials step2Credentials);

        Task<Step2Activity> CreateStep2ActivityUsingSecurityQuestions(Step2SecurityQuestions step2SecurityQuestions);

        Task<bool> UpdateStep2ActivityWithTruckTags(Step2TruckTagParameters step2TruckTagParameters);

        Task<string> GetStep2InspectionStatus(int step2ActivityId);

        Task<Step2InspectorData> GetStep2InspectorData(Step2InspectorCredentials step2InspectorCredentials);

        Task<int?> GetStep2TruckWaitingForInspection(Step2InspectionAuthentication step2InspectionAuthentication);

        Task UpdateStep2InspectionStatus(Step2InspectionStatus step2InspectionStatus);

        Task<Step2PrintedPassData> GetStep2PrintedPassData(int id);
    }
}