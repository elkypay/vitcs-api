﻿using System.Threading.Tasks;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.QueryParameters.Step5;

namespace Vitcs.Workstation.Dal.Abstract
{
    public interface IStep5Repository
    {
        Task<Step5Activity> CreateStep5Activity(Step5Credentials step5Credentials);

        Task<Step5Activity> CreateStep5ActivityUsingSecurityQuestions(Step5SecurityQuestions step5SecurityQuestions);

        Task<Step5WaybillData> GetStep5WaybillData(int id);
    }
}