﻿using System.Threading.Tasks;
using Vitcs.Workstation.Dal.Entities;

namespace Vitcs.Workstation.Dal.Abstract
{
    public interface IAuthenticationModeRepository
    {
        Task<AuthenticationMode> GetAuthenticationMode();
    }
}