﻿using System.Threading.Tasks;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.QueryParameters.Step3;

namespace Vitcs.Workstation.Dal.Abstract
{
    public interface IStep3Repository
    {
        Task<Step3Activity> CreateStep3Activity(Step3Credentials step3Credentials);

        Task<Step3Activity> CreateStep3ActivityUsingSecurityQuestions(Step3SecurityQuestions step3SecurityQuestions);

        Task<Step3ActivityOilData> GetStep3ActivityOilData(int id);
    }
}