﻿using System.Threading.Tasks;

namespace Vitcs.Workstation.Dal.Abstract
{
    public interface IDriverRepository
    {
        Task<bool> DriverWithAccessCardExists(string accessCard);
    }
}