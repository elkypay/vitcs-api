﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vitcs.Workstation.Dal.Abstract
{
    public interface ICountryRepository
    {
        Task<IEnumerable<string>> GetCountries();
    }
}