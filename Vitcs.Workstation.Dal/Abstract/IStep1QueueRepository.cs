﻿using System.Threading.Tasks;
using Vitcs.Workstation.Dal.Entities.Complex;

namespace Vitcs.Workstation.Dal.Abstract
{
    public interface IStep1QueueRepository
    {
        Task<Step1QueueData> GetStep1QueueData();
    }
}