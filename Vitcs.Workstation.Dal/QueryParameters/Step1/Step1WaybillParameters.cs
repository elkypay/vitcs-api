﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step1
{
    public class Step1WaybillParameters
    {
        public int Step1ActivityId { get; set; }

        public string Waybill { get; set; }

        public string WaybillInputMethod { get; set; }
    }
}