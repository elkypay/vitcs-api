﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step1
{
    public class Step1Credentials
    {
        public string AccessCard { get; set; }

        public bool IsManuallyEnteredAccessCard { get; set; }

        public string FingerprintId { get; set; }
    }
}