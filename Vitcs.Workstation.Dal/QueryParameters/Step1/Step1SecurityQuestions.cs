﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step1
{
    public class Step1SecurityQuestions
    {
        public string IqamaNo { get; set; }

        public string LicenseNo { get; set; }

        public string Country { get; set; }
    }
}