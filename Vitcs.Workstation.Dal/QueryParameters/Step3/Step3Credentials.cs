﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step3
{
    public class Step3Credentials
    {
        public int StationId { get; set; }

        public string Pass { get; set; }

        public bool IsManuallyEnteredPass { get; set; }
    }
}