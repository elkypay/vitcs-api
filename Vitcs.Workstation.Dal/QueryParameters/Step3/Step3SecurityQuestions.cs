﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step3
{
    public class Step3SecurityQuestions
    {
        public int StationId { get; set; }

        public string IqamaNo { get; set; }

        public string LicenseNo { get; set; }

        public string Country { get; set; }
    }
}