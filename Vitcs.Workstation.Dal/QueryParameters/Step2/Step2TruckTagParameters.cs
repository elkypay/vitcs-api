﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step2
{
    public class Step2TruckTagParameters
    {
        public int Step2ActivityId { get; set; }

        public string TruckTag { get; set; }

        public string TruckContainerTag { get; set; }

        public bool IsManuallyEnteredTruckTags { get; set; }
    }
}