﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step2
{
    public class Step2InspectionAuthentication
    {
        public string DriverTagId { get; set; }

        public string DriverFullName { get; set; }

        public string DriverIqamaNo { get; set; }

        public string TruckTagId { get; set; }

        public string TruckContainerTagId { get; set; }
    }
}