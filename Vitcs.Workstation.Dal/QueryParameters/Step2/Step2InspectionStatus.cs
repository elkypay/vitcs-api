﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step2
{
    public class Step2InspectionStatus
    {
        public int Step2ActivityId { get; set; }

        public int InspectorId { get; set; }

        public string InspectionStatus { get; set; }

        public string InspectionComment { get; set; }
    }
}