﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step2
{
    public class Step2InspectorCredentials
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}