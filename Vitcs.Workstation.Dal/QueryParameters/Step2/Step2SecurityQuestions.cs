﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step2
{
    public class Step2SecurityQuestions
    {
        public string IqamaNo { get; set; }

        public string LicenseNo { get; set; }

        public string Country { get; set; }
    }
}