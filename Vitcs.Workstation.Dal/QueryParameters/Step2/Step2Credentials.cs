﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step2
{
    public class Step2Credentials
    {
        public string AccessCard { get; set; }

        public bool IsManuallyEnteredAccessCard { get; set; }
    }
}