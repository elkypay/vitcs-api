﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step5
{
    public class Step5Credentials
    {
        public string Pass { get; set; }

        public bool IsManuallyEnteredPass { get; set; }
    }
}