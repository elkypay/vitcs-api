﻿namespace Vitcs.Workstation.Dal.QueryParameters.Step5
{
    public class Step5SecurityQuestions
    {
        public string IqamaNo { get; set; }

        public string LicenseNo { get; set; }

        public string Country { get; set; }
    }
}