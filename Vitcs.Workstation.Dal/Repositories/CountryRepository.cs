﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Helpers;

namespace Vitcs.Workstation.Dal.Repositories
{
    public class CountryRepository : ICountryRepository
    {
        private readonly string _connectionString;

        public CountryRepository(string connectionString)
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<IEnumerable<string>> GetCountries()
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QueryAsync<string>("GetCountries", commandType: CommandType.StoredProcedure);
        }
    }
}