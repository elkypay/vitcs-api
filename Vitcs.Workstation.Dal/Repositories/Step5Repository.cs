﻿using System;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.Helpers;
using Vitcs.Workstation.Dal.QueryParameters.Step5;

namespace Vitcs.Workstation.Dal.Repositories
{
    public class Step5Repository : IStep5Repository
    {
        private readonly string _connectionString;

        public Step5Repository(string connectionString)
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<Step5Activity> CreateStep5Activity(Step5Credentials step5Credentials)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
            {
                SqlMapper.GridReader gridReader = await db.QueryMultipleAsync("CreateStep5Activity",
                    step5Credentials.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
                return new Step5Activity
                {
                    Id = await gridReader.ReadSingleAsync<int?>(),
                    DriverFullName = await gridReader.ReadSingleOrDefaultAsync<string>()
                };
            }
        }

        public async Task<Step5Activity> CreateStep5ActivityUsingSecurityQuestions(Step5SecurityQuestions step5SecurityQuestions)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
            {
                SqlMapper.GridReader gridReader = await db.QueryMultipleAsync("CreateStep5ActivityUsingSecurityQuestions",
                    step5SecurityQuestions.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
                return new Step5Activity
                {
                    Id = await gridReader.ReadSingleAsync<int?>(),
                    DriverFullName = await gridReader.ReadSingleOrDefaultAsync<string>()
                };
            }
        }

        public async Task<Step5WaybillData> GetStep5WaybillData(int id)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QuerySingleAsync<Step5WaybillData>("GetStep5WaybillData", new { id },
                    commandType: CommandType.StoredProcedure);
        }
    }
}