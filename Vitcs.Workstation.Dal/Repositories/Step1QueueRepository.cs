﻿using System;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.Helpers;

namespace Vitcs.Workstation.Dal.Repositories
{
    public class Step1QueueRepository : IStep1QueueRepository
    {
        private readonly string _connectionString;

        public Step1QueueRepository(string connectionString)
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<Step1QueueData> GetStep1QueueData()
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
            {
                SqlMapper.GridReader gridReader = await db.QueryMultipleAsync("GetStep1QueueData", commandType: CommandType.StoredProcedure);
                return new Step1QueueData
                {
                    PendingQueueNumbers = await gridReader.ReadAsync<int>(),
                    ReadyQueueNumbers = await gridReader.ReadAsync<int>()
                };
            }
        }
    }
}