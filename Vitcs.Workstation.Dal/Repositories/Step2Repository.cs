﻿using System;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.Helpers;
using Vitcs.Workstation.Dal.QueryParameters.Step2;

namespace Vitcs.Workstation.Dal.Repositories
{
    public class Step2Repository : IStep2Repository
    {
        private readonly string _connectionString;

        public Step2Repository(string connectionString)
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<Step2Activity> CreateStep2Activity(Step2Credentials step2Credentials)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
            {
                SqlMapper.GridReader gridReader = await db.QueryMultipleAsync("CreateStep2Activity",
                    step2Credentials.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
                return new Step2Activity
                {
                    Id = await gridReader.ReadSingleAsync<int?>(),
                    DriverFullName = await gridReader.ReadSingleOrDefaultAsync<string>()
                };
            }
        }

        public async Task<Step2Activity> CreateStep2ActivityUsingSecurityQuestions(Step2SecurityQuestions step2SecurityQuestions)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
            {
                SqlMapper.GridReader gridReader = await db.QueryMultipleAsync("CreateStep2ActivityUsingSecurityQuestions",
                    step2SecurityQuestions.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
                return new Step2Activity
                {
                    Id = await gridReader.ReadSingleAsync<int?>(),
                    DriverFullName = await gridReader.ReadSingleOrDefaultAsync<string>()
                };
            }
        }

        public async Task<bool> UpdateStep2ActivityWithTruckTags(Step2TruckTagParameters step2TruckTagParameters)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QuerySingleAsync<bool>("UpdateStep2ActivityWithTruckTags",
                    step2TruckTagParameters.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
        }

        public async Task<string> GetStep2InspectionStatus(int step2ActivityId)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QuerySingleAsync<string>("GetStep2InspectionStatus", new { step2ActivityId },
                    commandType: CommandType.StoredProcedure);
        }

        public async Task<Step2InspectorData> GetStep2InspectorData(Step2InspectorCredentials step2InspectorCredentials)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QuerySingleOrDefaultAsync<Step2InspectorData>("GetStep2InspectorData",
                    step2InspectorCredentials.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
        }

        public async Task<int?> GetStep2TruckWaitingForInspection(Step2InspectionAuthentication step2InspectionAuthentication)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QuerySingleOrDefaultAsync<int?>("GetStep2TruckWaitingForInspection",
                    step2InspectionAuthentication.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
        }

        public async Task UpdateStep2InspectionStatus(Step2InspectionStatus step2InspectionStatus)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                await db.ExecuteAsync("UpdateStep2InspectionStatus", step2InspectionStatus.ToDynamicParameters(),
                    commandType: CommandType.StoredProcedure);
        }

        public async Task<Step2PrintedPassData> GetStep2PrintedPassData(int id)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QuerySingleOrDefaultAsync<Step2PrintedPassData>("GetStep2PrintedPassData", new { id },
                    commandType: CommandType.StoredProcedure);
        }
    }
}