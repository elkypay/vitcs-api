﻿using System;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Helpers;

namespace Vitcs.Workstation.Dal.Repositories
{
    public class DriverRepository : IDriverRepository
    {
        private readonly string _connectionString;

        public DriverRepository(string connectionString)
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<bool> DriverWithAccessCardExists(string accessCard)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QuerySingleAsync<int>("SELECT COUNT(*) FROM Drivers WHERE TagId = @accessCard", new { accessCard }) > 0;
        }
    }
}