﻿using System;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Entities;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.Helpers;
using Vitcs.Workstation.Dal.QueryParameters.Step1;

namespace Vitcs.Workstation.Dal.Repositories
{
    public class Step1Repository : IStep1Repository
    {
        private readonly string _connectionString;

        public Step1Repository(string connectionString)
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<Step1Activity> CreateStep1Activity(Step1Credentials step1Credentials)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
            {
                SqlMapper.GridReader gridReader = await db.QueryMultipleAsync("CreateStep1Activity",
                    step1Credentials.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
                return new Step1Activity
                {
                    Id = await gridReader.ReadSingleAsync<int?>(),
                    DriverFullName = await gridReader.ReadSingleOrDefaultAsync<string>()
                };
            }
        }

        public async Task<Step1Activity> CreateStep1ActivityUsingSecurityQuestions(Step1SecurityQuestions step1SecurityQuestions)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
            {
                SqlMapper.GridReader gridReader = await db.QueryMultipleAsync("CreateStep1ActivityUsingSecurityQuestions",
                    step1SecurityQuestions.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
                return new Step1Activity
                {
                    Id = await gridReader.ReadSingleAsync<int?>(),
                    DriverFullName = await gridReader.ReadSingleOrDefaultAsync<string>()
                };
            }
        }

        public async Task<OperationResult> UpdateStep1ActivityWithWaybill(Step1WaybillParameters step1WaybillParameters)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QuerySingleAsync<OperationResult>("UpdateStep1ActivityWithWaybill",
                    step1WaybillParameters.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
        }

        public async Task<Step1PrintedPassData> GetStep1PrintedPassData(int id)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QuerySingleOrDefaultAsync<Step1PrintedPassData>("GetStep1PrintedPassData", new { id }, 
                    commandType: CommandType.StoredProcedure);
        }
    }
}