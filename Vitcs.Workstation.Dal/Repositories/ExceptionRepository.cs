﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Helpers;
using Exception = Vitcs.Workstation.Dal.Entities.Exception;

namespace Vitcs.Workstation.Dal.Repositories
{
    public class ExceptionRepository : IExceptionRepository
    {
        private readonly string _connectionString;

        public ExceptionRepository(string connectionString)
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task SaveException(IEnumerable<Exception> exceptions)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
            {
                var ex = exceptions.AsTableValuedParameter("ExceptionTableType");

                await db.ExecuteAsync("LogException",
                    new { exceptions = ex },
                    commandType: CommandType.StoredProcedure);
            }
        }
    }
}
