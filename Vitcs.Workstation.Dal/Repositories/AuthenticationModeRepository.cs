﻿using System;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Entities;
using Vitcs.Workstation.Dal.Helpers;

namespace Vitcs.Workstation.Dal.Repositories
{
    public class AuthenticationModeRepository : IAuthenticationModeRepository
    {
        private readonly string _connectionString;

        public AuthenticationModeRepository(string connectionString)
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<AuthenticationMode> GetAuthenticationMode()
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QuerySingleAsync<AuthenticationMode>("GetAuthenticationMode", new { stepId = 1 }, commandType: CommandType.StoredProcedure);
        }
    }
}