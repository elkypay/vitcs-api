﻿using System;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.Helpers;
using Vitcs.Workstation.Dal.QueryParameters.Step3;

namespace Vitcs.Workstation.Dal.Repositories
{
    public class Step3Repository : IStep3Repository
    {
        private readonly string _connectionString;

        public Step3Repository(string connectionString)
        {
            _connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<Step3Activity> CreateStep3Activity(Step3Credentials step3Credentials)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
            {
                SqlMapper.GridReader gridReader = await db.QueryMultipleAsync("CreateStep3Activity",
                    step3Credentials.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
                return new Step3Activity
                {
                    Id = await gridReader.ReadSingleAsync<int?>(),
                    DriverFullName = await gridReader.ReadSingleOrDefaultAsync<string>()
                };
            }
        }

        public async Task<Step3Activity> CreateStep3ActivityUsingSecurityQuestions(Step3SecurityQuestions step3SecurityQuestions)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
            {
                SqlMapper.GridReader gridReader = await db.QueryMultipleAsync("CreateStep3ActivityUsingSecurityQuestions",
                    step3SecurityQuestions.ToDynamicParameters(), commandType: CommandType.StoredProcedure);
                return new Step3Activity
                {
                    Id = await gridReader.ReadSingleAsync<int?>(),
                    DriverFullName = await gridReader.ReadSingleOrDefaultAsync<string>()
                };
            }
        }

        public async Task<Step3ActivityOilData> GetStep3ActivityOilData(int id)
        {
            using (IDbConnection db = SqlConnectionHelper.GetDbConnection(_connectionString))
                return await db.QuerySingleAsync<Step3ActivityOilData>("GetStep3ActivityOilData", new { id },
                    commandType: CommandType.StoredProcedure);
        }
    }
}