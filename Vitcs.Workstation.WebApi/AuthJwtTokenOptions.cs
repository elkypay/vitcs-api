﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Vitcs.Workstation.WebApi
{
    public static class AuthJwtTokenOptions
    {
        public const string Issuer = "VitcsAuthServer";

        private const string Key = "supersecret_secretkey!12345";

        public static SecurityKey GetSecurityKey() =>
            new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
    }
}