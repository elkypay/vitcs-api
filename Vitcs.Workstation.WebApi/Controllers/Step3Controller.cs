﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.QueryParameters.Step3;

namespace Vitcs.Workstation.WebApi.Controllers
{
    [Route("step3")]
    [ApiController]
    public class Step3Controller : ControllerBase
    {
        private readonly ICountryRepository _countryRepository;

        private readonly IStep3Repository _step3Repository;

        private static readonly Random _random = new Random();

        public Step3Controller(
            ICountryRepository countryRepository,
            IStep3Repository step3Repository)
        {
            _countryRepository = countryRepository ?? throw new ArgumentNullException(nameof(countryRepository));

            _step3Repository = step3Repository ?? throw new ArgumentNullException(nameof(step3Repository));
        }

        /// <summary>
        /// Creates Step 3 activity based on entered credentials.
        /// </summary>
        /// <param name="step3Credentials"></param>
        /// <returns></returns>
        [HttpPost("activity")]
        public async Task<ActionResult<Step3Activity>> CreateStep1Activity([FromBody]Step3Credentials step3Credentials) =>
            await _step3Repository.CreateStep3Activity(step3Credentials);

        /// <summary>
        /// Gets list of countries.
        /// </summary>
        /// <returns></returns>
        [HttpGet("countries")]
        public async Task<IActionResult> GetCountries() => Ok(await _countryRepository.GetCountries());

        /// <summary>
        /// Creates Step 3 activity based on security question answers.
        /// </summary>
        /// <param name="step3SecurityQuestions"></param>
        /// <returns></returns>
        [HttpPost("activity-using-security-questions")]
        public async Task<ActionResult<Step3Activity>> CreateStep1ActivityUsingSecurityQuestions([FromBody]Step3SecurityQuestions step3SecurityQuestions) =>
            await _step3Repository.CreateStep3ActivityUsingSecurityQuestions(step3SecurityQuestions);

        /// <summary>
        /// Gets oil data for Step 3.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("oil-data")]
        public async Task<ActionResult<Step3ActivityOilData>> GetStep3ActivityOilData(int id) =>
            await _step3Repository.GetStep3ActivityOilData(id);
    }
}