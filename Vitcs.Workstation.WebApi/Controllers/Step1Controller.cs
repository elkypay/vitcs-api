﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Entities;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.QueryParameters.Step1;

namespace Vitcs.Workstation.WebApi.Controllers
{
    [Route("step1")]
    [ApiController]
    public class Step1Controller : ControllerBase
    {
        private readonly IAuthenticationModeRepository _authenticationModeRepository;
        private readonly IDriverRepository _driverRepository;
        private readonly ICountryRepository _countryRepository;

        private readonly IStep1Repository _step1Repository;

        public Step1Controller(
            IAuthenticationModeRepository authenticationModeRepository,
            IDriverRepository driverRepository,
            ICountryRepository countryRepository,
            IStep1Repository step1Repository)
        {
            _authenticationModeRepository = authenticationModeRepository ?? throw new ArgumentNullException(nameof(authenticationModeRepository));
            _driverRepository = driverRepository ?? throw new ArgumentNullException(nameof(driverRepository));
            _countryRepository = countryRepository ?? throw new ArgumentNullException(nameof(countryRepository));

            _step1Repository = step1Repository ?? throw new ArgumentNullException(nameof(step1Repository));
        }

        /// <summary>
        /// Gets info about allowed authentication modes.
        /// </summary>
        /// <returns></returns>
        [HttpGet("authentication-mode")]
        public async Task<ActionResult<AuthenticationMode>> GetAuthenticationMode() =>
            await _authenticationModeRepository.GetAuthenticationMode();

        /// <summary>
        /// Checks existence of driver with specified access card.
        /// </summary>
        /// <param name="accessCard"></param>
        /// <returns>true or false</returns>
        [HttpGet("access-card-existence")]
        public async Task<ActionResult<bool>> DriverWithAccessCardExists(string accessCard) =>
            await _driverRepository.DriverWithAccessCardExists(accessCard);

        /// <summary>
        /// Creates Step 1 activity based on entered credentials.
        /// </summary>
        /// <param name="step1Credentials"></param>
        /// <returns></returns>
        [HttpPost("activity")]
        public async Task<ActionResult<Step1Activity>> CreateStep1Activity([FromBody]Step1Credentials step1Credentials) =>
            await _step1Repository.CreateStep1Activity(step1Credentials);

        /// <summary>
        /// Gets list of countries.
        /// </summary>
        /// <returns></returns>
        [HttpGet("countries")]
        public async Task<IActionResult> GetCountries() => Ok(await _countryRepository.GetCountries());

        /// <summary>
        /// Creates Step 1 activity based on security question answers.
        /// </summary>
        /// <param name="step1SecurityQuestions"></param>
        /// <returns></returns>
        [HttpPost("activity-using-security-questions")]
        public async Task<ActionResult<Step1Activity>> CreateStep1ActivityUsingSecurityQuestions([FromBody]Step1SecurityQuestions step1SecurityQuestions) =>
            await _step1Repository.CreateStep1ActivityUsingSecurityQuestions(step1SecurityQuestions);

        /// <summary>
        /// Updates Step 1 activity with waybill info.
        /// </summary>
        /// <param name="step1WaybillParameters"></param>
        /// <returns></returns>
        [HttpPatch("activity-waybill")]
        public async Task<ActionResult<OperationResult>> UpdateStep1ActivityWithWaybill([FromBody]Step1WaybillParameters step1WaybillParameters) =>
            await _step1Repository.UpdateStep1ActivityWithWaybill(step1WaybillParameters);

        /// <summary>
        /// Gets data to be printed on the pass after Step 1.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("printed-pass-data")]
        public async Task<ActionResult<Step1PrintedPassData>> GetStep1PrintedPassData(int id) =>
            await _step1Repository.GetStep1PrintedPassData(id);
    }
}