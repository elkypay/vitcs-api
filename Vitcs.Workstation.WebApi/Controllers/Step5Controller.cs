﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.QueryParameters.Step5;

namespace Vitcs.Workstation.WebApi.Controllers
{
    [Route("step5")]
    [ApiController]
    public class Step5Controller : ControllerBase
    {
        private readonly ICountryRepository _countryRepository;

        private readonly IStep5Repository _step5Repository;

        private static readonly Random _random = new Random();

        public Step5Controller(
            ICountryRepository countryRepository,
            IStep5Repository step5Repository)
        {
            _countryRepository = countryRepository ?? throw new ArgumentNullException(nameof(countryRepository));

            _step5Repository = step5Repository ?? throw new ArgumentNullException(nameof(step5Repository));
        }

        /// <summary>
        /// Creates Step 5 activity based on entered credentials.
        /// </summary>
        /// <param name="step5Credentials"></param>
        /// <returns></returns>
        [HttpPost("activity")]
        public async Task<ActionResult<Step5Activity>> CreateStep1Activity([FromBody]Step5Credentials step5Credentials) =>
            await _step5Repository.CreateStep5Activity(step5Credentials);

        /// <summary>
        /// Gets list of countries.
        /// </summary>
        /// <returns></returns>
        [HttpGet("countries")]
        public async Task<IActionResult> GetCountries() => Ok(await _countryRepository.GetCountries());

        /// <summary>
        /// Creates Step 5 activity based on security question answers.
        /// </summary>
        /// <param name="step5SecurityQuestions"></param>
        /// <returns></returns>
        [HttpPost("activity-using-security-questions")]
        public async Task<ActionResult<Step5Activity>> CreateStep1ActivityUsingSecurityQuestions([FromBody]Step5SecurityQuestions step5SecurityQuestions) =>
            await _step5Repository.CreateStep5ActivityUsingSecurityQuestions(step5SecurityQuestions);

        /// <summary>
        /// Gets data to be printed on the waybill after Step 5.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("waybill-data")]
        public async Task<ActionResult<Step5WaybillData>> GetStep5WaybillData(int id) =>
            await _step5Repository.GetStep5WaybillData(id);
    }
}