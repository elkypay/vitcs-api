﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Entities.Complex;
using Vitcs.Workstation.Dal.QueryParameters.Step2;

namespace Vitcs.Workstation.WebApi.Controllers
{
    [Route("step2")]
    [ApiController]
    public class Step2Controller : ControllerBase
    {
        private readonly ICountryRepository _countryRepository;

        private readonly IStep2Repository _step2Repository;

        public Step2Controller(
            ICountryRepository countryRepository,
            IStep2Repository step2Repository)
        {
            _countryRepository = countryRepository ?? throw new ArgumentNullException(nameof(countryRepository));

            _step2Repository = step2Repository ?? throw new ArgumentNullException(nameof(step2Repository));
        }

        /// <summary>
        /// Creates Step 2 activity based on entered credentials.
        /// </summary>
        /// <param name="step2Credentials"></param>
        /// <returns></returns>
        [HttpPost("activity")]
        public async Task<ActionResult<Step2Activity>> CreateStep1Activity([FromBody]Step2Credentials step2Credentials) =>
            await _step2Repository.CreateStep2Activity(step2Credentials);

        /// <summary>
        /// Gets list of countries.
        /// </summary>
        /// <returns></returns>
        [HttpGet("countries")]
        public async Task<IActionResult> GetCountries() => Ok(await _countryRepository.GetCountries());

        /// <summary>
        /// Creates Step 2 activity based on security question answers.
        /// </summary>
        /// <param name="step2SecurityQuestions"></param>
        /// <returns></returns>
        [HttpPost("activity-using-security-questions")]
        public async Task<ActionResult<Step2Activity>> CreateStep1ActivityUsingSecurityQuestions([FromBody]Step2SecurityQuestions step2SecurityQuestions) =>
            await _step2Repository.CreateStep2ActivityUsingSecurityQuestions(step2SecurityQuestions);

        /// <summary>
        /// Updates Step 2 activity with truck tags.
        /// </summary>
        /// <param name="step2TruckTagParameters"></param>
        /// <returns></returns>
        [HttpPatch("activity-truck-tags")]
        public async Task<ActionResult<bool>> UpdateStep2ActivityWithTruckTags([FromBody]Step2TruckTagParameters step2TruckTagParameters) =>
            await _step2Repository.UpdateStep2ActivityWithTruckTags(step2TruckTagParameters);

        /// <summary>
        /// Gets inspection status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("inspection-status")]
        public async Task<ActionResult<string>> GetStep2InspectionStatus(int id)
        {
            string status = await _step2Repository.GetStep2InspectionStatus(id);
            return status == null ? null : $"\"{status}\"";
        }

        /// <summary>
        /// Gets inspector data from credentials
        /// </summary>
        /// <param name="inspectorCredentials"></param>
        /// <returns></returns>
        [HttpPost("inspector-login")]
        public async Task<ActionResult<Step2InspectorData>> GetInspectorData([FromBody]Step2InspectorCredentials inspectorCredentials) =>
            await _step2Repository.GetStep2InspectorData(inspectorCredentials);

        /// <summary>
        /// Gets truck waiting for inspection
        /// </summary>
        /// <param name="driverTagId"></param>
        /// <param name="driverFullName"></param>
        /// <param name="driverIqamaNo"></param>
        /// <param name="truckTagId"></param>
        /// <param name="truckContainerTagId"></param>
        /// <returns></returns>
        [HttpGet("activity-for-inspection")]
        public async Task<ActionResult<int?>> GetStep2TruckWaitingForInspection(string driverTagId, string driverFullName, string driverIqamaNo, string truckTagId, string truckContainerTagId) =>
            await _step2Repository.GetStep2TruckWaitingForInspection(new Step2InspectionAuthentication
            {
                DriverTagId = driverTagId,
                DriverFullName = driverFullName,
                DriverIqamaNo = driverIqamaNo,
                TruckTagId = truckTagId,
                TruckContainerTagId = truckContainerTagId
            });

        /// <summary>
        /// Update inspection status
        /// </summary>
        /// <param name="step2InspectionStatus"></param>
        /// <returns></returns>
        [HttpPatch("activity-inspection-status")]
        public async Task<IActionResult> UpdateStep2InspectionStatus([FromBody]Step2InspectionStatus step2InspectionStatus)
        {
            await _step2Repository.UpdateStep2InspectionStatus(step2InspectionStatus);
            return Ok();
        }

        /// <summary>
        /// Gets data to be printed on the pass after Step 2.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("printed-pass-data")]
        public async Task<ActionResult<Step2PrintedPassData>> GetStep2PrintedPassData(int id) =>
            await _step2Repository.GetStep2PrintedPassData(id);
    }
}