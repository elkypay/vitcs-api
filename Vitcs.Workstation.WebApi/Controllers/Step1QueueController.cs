﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Entities.Complex;

namespace Vitcs.Workstation.WebApi.Controllers
{
    [Route("step1-queue")]
    [ApiController]
    public class Step1QueueController : ControllerBase
    {
        private readonly IStep1QueueRepository _step1QueueRepository;

        public Step1QueueController(IStep1QueueRepository step1QueueRepository)
        {
            _step1QueueRepository = step1QueueRepository ?? throw new ArgumentNullException(nameof(step1QueueRepository));
        }

        /// <summary>
        /// Gets current queue state.
        /// </summary>
        /// <returns></returns>
        [HttpGet("queue-data")]
        public async Task<ActionResult<Step1QueueData>> GetStep1QueueData() =>
            await _step1QueueRepository.GetStep1QueueData();
    }
}