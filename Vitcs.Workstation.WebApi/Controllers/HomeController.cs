﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Vitcs.Workstation.Dal.Abstract;
using Exception = Vitcs.Workstation.Dal.Entities.Exception;

namespace Vitcs.Workstation.WebApi.Controllers
{
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IExceptionRepository _exceptionRepository;

        public HomeController(IExceptionRepository exceptionRepository)
        {
            _exceptionRepository = exceptionRepository ?? throw new ArgumentNullException(nameof(exceptionRepository));
        }

        /// <summary>
        /// Saves error from workstation client.
        /// </summary>
        /// <returns></returns>
        [HttpPost("/error")]
        public async Task<IActionResult> SaveError([FromBody]System.Exception unhandledException)
        {
            Stack<Exception> exceptions = new Stack<Exception>();

            do
            {
                exceptions.Push(ToDbException(unhandledException));
                unhandledException = unhandledException.InnerException;
            }
            while (unhandledException != null);

            await _exceptionRepository.SaveException(exceptions);

            Exception ToDbException(System.Exception exception)
            {
                return new Exception
                {
                    ExceptionSource = "Workstation client",
                    Message = exception.Message,
                    StackTrace = exception.StackTrace,
                    Type = exception.GetType().FullName
                };
            }

            return Ok();
        }

        /// <summary>
        /// Used internally for saving server errors
        /// </summary>
        /// <returns></returns>
        [HttpPost("/internal-error")]
        public async Task<IActionResult> SaveInternalError()
        {
            IExceptionHandlerPathFeature exceptionFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exceptionFeature != null)
            {
                string route = exceptionFeature.Path;

                System.Exception unhandledException = exceptionFeature.Error;

                Stack<Exception> exceptions = new Stack<Exception>();

                do
                {
                    exceptions.Push(ToDbException(unhandledException));
                    unhandledException = unhandledException.InnerException;
                }
                while (unhandledException != null);

                Exception ToDbException(System.Exception exception)
                {
                    return new Exception
                    {
                        ExceptionSource = "Workstation API",
                        Route = route,
                        Message = exception.Message,
                        StackTrace = exception.StackTrace,
                        Type = exception.GetType().FullName
                    };
                }

                await _exceptionRepository.SaveException(exceptions);
            }

            return StatusCode((int)HttpStatusCode.InternalServerError, "Internal server error");
        }
    }
}