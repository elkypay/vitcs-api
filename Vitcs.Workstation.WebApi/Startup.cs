﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Vitcs.Workstation.Dal.Abstract;
using Vitcs.Workstation.Dal.Repositories;

namespace Vitcs.Workstation.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            string connectionString = Configuration.GetConnectionString("VITCS");

            services.AddScoped<IExceptionRepository>(sp => new ExceptionRepository(connectionString));

            services.AddScoped<IAuthenticationModeRepository>(sp => new AuthenticationModeRepository(connectionString));
            services.AddScoped<IDriverRepository>(sp => new DriverRepository(connectionString));
            services.AddScoped<ICountryRepository>(sp => new CountryRepository(connectionString));

            services.AddScoped<IStep1Repository>(sp => new Step1Repository(connectionString));
            services.AddScoped<IStep1QueueRepository>(sp => new Step1QueueRepository(connectionString));
            services.AddScoped<IStep2Repository>(sp => new Step2Repository(connectionString));
            services.AddScoped<IStep3Repository>(sp => new Step3Repository(connectionString));
            services.AddScoped<IStep5Repository>(sp => new Step5Repository(connectionString));

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "VITCS Workstation API",
                    Version = "v1"
                });

                // Set the comments path for the Swagger JSON and UI.
                string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFile));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler("/internal-error");

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "VITCS Workstation API V1");
            });

            app.UseAuthentication();
            app.UseMvc();
        }
    }
}